# Project Title

AI Active Small Test

## Description

# Build a React Tree component that must have the following requirements:

-   The tree supports n levels.
-   It can select/expand/collapse a node.
-   It has the ability to override default methods for expanding/collapsing/selecting nodes.
-   It can search a node by name.
-   It supports fetching data via API calls.
-   It supports lazy loading (i.e. load children of a node when expanding).
-   It supports "load more children" function (i.e. when a node has large children then it must be paginated to increase performance)
-   It can integrate with Redux
-   It supports virtualized rendering (nice to have)

## DEMO

![](https://media.giphy.com/media/0muVGkywuZenULXWvr/giphy.gif)

## Getting Started

### Dependencies

-   **[React](https://facebook.github.io/react/)** (17.x)
-   **[Webpack](https://webpack.js.org/)** (5.x)
-   **[Typescript](https://www.typescriptlang.org/)** (4.x)
-   Code linting ([ESLint](https://github.com/eslint/eslint)) and formatting ([Prettier](https://github.com/prettier/prettier))
-   etc

### Installing

1. Clone/download repo
2. `yarn install` (or `npm install` for npm)

---

### How to Start

#### Backend: http://localhost:3030

`yarn start:server`

#### Frontend: http://localhost:4000

`yarn start`

---
