const express = require("express");
const cors = require("cors");
const faker = require("faker");
const _ = require("lodash");


faker.seed(123);

const app = express();

app.use(cors());

app.use(express.json());

let index = 0;

const generateCity = (amount) => {
    return _.times(amount, (i) => { 
        const city = faker.address.city();
        return {
            index: index++,
            label: city,
            value: city.replace(/\s+/g, '-').toLowerCase(),
            children: (faker.datatype.boolean() === true ? generateStreet(faker.datatype.number({min: 1, max: 3})) : undefined)
        }  
    });
}

const generateStreet = (amount) => {
    return _.times(amount, (i) => { 
        const street = faker.address.streetName();
        return {
            index: index++,
            label: street,
            value: street.replace(/\s+/g, '-').toLowerCase(),
            children: (faker.datatype.boolean() === true ? generateName(faker.datatype.number({min: 1, max: 4})) : undefined)
        }  
    });
}

const generateName = (amount) => {
    return _.times(amount, (i) => { 
        const name = faker.fake('{{name.lastName}}, {{name.firstName}} {{name.suffix}}');
        return {
            index: index++,
            label: name,
            value: name.replace(/\s+/g, '-').toLowerCase(),
        }  
    });
}

//-------------metadata--------------
const data = _.times(3, (i) => {
    const job = `${i + 1}-${faker.name.jobTitle()}`
    return {
      index: index++,
      label: job,
      value: job.replace(/\s+/g, '-').toLowerCase(),
      children: generateCity(faker.datatype.number({min: 1, max: 2})),
    };
  });
  //-------------functions--------------
  console.log("===============INIT-SAMPLE-DATA-SUCCESS==============");
  console.log('data', data);
  console.log('JSON', JSON.stringify(data));
  
  app.listen(3030, () => {
    console.log("server started on port 3030");
  });
  

  app.get("/api/get_data", (req, res) => {
    return res.status(200).send({
      reply: data,
      result: "success",
    });
  });

  app.get("/api/loadmore", (req, res) => {
      const loadmoreData = generateName(5);
    return res.status(200).send({
      reply: loadmoreData,
      result: "success",
    });
  });