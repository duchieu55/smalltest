import treeAction from './constants';
import { TreePayLoad } from './actions';
import { ReduxAction } from '../type';
import { NodeProps } from '../../components/TreeNode';

export type TreeState = {
    treeInfo: NodeProps[];
};

export const defaultState: TreeState = {
    treeInfo: [],
};

const treeReducer = (state: TreeState = defaultState, action: ReduxAction<any>): TreeState => {
    const { type: actionType } = action;

    switch (actionType) {
        case treeAction.TREE_DATA_RECEIVED: {
            const actionPayload = (action as ReduxAction<TreePayLoad>).payload;
            const { treeInfo } = actionPayload;

            return {
                ...state,
                treeInfo,
            };
        }

        default: {
            return state;
        }
    }
};

export default treeReducer;
