import { Dispatch } from 'redux';
import axios from 'axios';
import { ReduxAction } from '../type';
import treeAction from './constants';
import { SUCCESS_STATUS } from '../../constants/contants';
import { NodeProps } from '../../components/LemonTree/components/TreeNode';

//===========================================================
export type TreePayLoad = { treeInfo: NodeProps[] };
//===========================================================

export const treeDataReceived = (treeInfo: any): ReduxAction<TreePayLoad> => {
    return {
        type: treeAction.TREE_DATA_RECEIVED,
        payload: { treeInfo },
    };
};

export const fetchTreeData = () => {
    return async (dispatch: Dispatch<ReduxAction<TreePayLoad>>) => {
        try {
            const response = await axios.get('http://localhost:3030/api/get_data');
            if (response.data.result === SUCCESS_STATUS) {
                dispatch(treeDataReceived(response.data.reply));
            }
        } catch (err) {
            // handle error
        }
    };
};
