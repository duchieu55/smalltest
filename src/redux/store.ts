import { compose, createStore, applyMiddleware } from 'redux';
import { createLogger } from 'redux-logger';
import storage from 'redux-persist/lib/storage';
import { persistStore, persistReducer } from 'redux-persist';
import thunkMiddleware from 'redux-thunk';
import rootReducer from './rootReducer';

const loggerMiddleware = createLogger();

const persistConfig = {
    // Root
    key: 'root',
    storage,
    // Whitelist (Save Specific Reducers)
    // whitelist: ['auth'],
    // Blacklist (Don't Save Specific Reducers)
};
// Middleware: Redux Persist Persisted Reducer
const persistedReducer = persistReducer(persistConfig, rootReducer);

export const store: any = createStore(
    persistedReducer,
    undefined,
    compose(
        applyMiddleware(thunkMiddleware),
        applyMiddleware(loggerMiddleware)
        // window.__REDUX_DEVTOOLS_EXTENSION__ &&
        // window.__REDUX_DEVTOOLS_EXTENSION__(),
    )
);

// Infer the `RootState` and `AppDispatch` types from the store itself
export type RootState = ReturnType<typeof store.getState>;
// Inferred type: {posts: PostsState, comments: CommentsState, users: UsersState}
export type AppDispatch = typeof store.dispatch;

export const persist = persistStore(store);

export default store;
