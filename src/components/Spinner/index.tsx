import React from 'react';
import './style.scss';

const Spinner: React.FC = () => {
    return (
        <div className="loadingio-spinner-pulse-ug80c6zex1">
            <div className="ldio-pq0n6qtbgke">
                <div></div>
                <div></div>
                <div></div>
            </div>
        </div>
    );
};

export default Spinner;
