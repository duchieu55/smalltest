import { isEmpty } from '../../utils';
import { NodeProps } from './components/TreeNode';

export const isLeaf = (children: string | any[]) => {
    return isEmpty(children);
};

export const isLastChildren = (node: NodeProps, listNodesTree: Record<string, NodeProps>) => {
    if (isLeaf(node._children) && node.parent) {
        const parentNode = listNodesTree[node.parent];
        if (parentNode._children && parentNode._children[parentNode._children.length - 1] === node.id) {
            return true;
        }
    }
    return false;
};

export const shouldRenderNode = (node: NodeProps, data: Record<string, NodeProps>, searchModeOn?: boolean) => {
    const { expanded, parent } = node;
    if (searchModeOn || expanded) return true;
    if (expanded) return true;

    const parentData = parent && data[parent];
    // if it has a parent, then check parent's state.
    // otherwise root nodes are always rendered
    return !parentData || parentData.expanded;
};

export function flattenTree(tree: any, rootPrefixId: string) {
    const forest = Array.isArray(tree) ? tree : [tree];
    let source: Record<string, NodeProps> = {};
    const initDepth = 0;

    // eslint-disable-next-line no-use-before-define
    return walkNodes(forest, rootPrefixId, source, initDepth);
}

export const walkNodes = (
    nodes: NodeProps[],
    rootPrefixId: string,
    source: Record<string, NodeProps> = {},
    depth: number,
    parent?: NodeProps
) => {
    nodes.forEach((node, i) => {
        node.depth = depth;
        if (parent) {
            node.id = node.id || `${parent.id}-${i}`;
            node.parent = parent.id;
            node.childrenIndex = i;
            parent._children.push(node.id);
            source[parent.id] = { ...parent };
        } else {
            node.id = node.id || `${rootPrefixId ? `${rootPrefixId}-${i}` : i}`;
        }
        const { id } = node;
        source[id] = { ...node };

        if (node.children) {
            node._children = [];
            walkNodes(node.children, rootPrefixId, source, depth + 1, node);

            node.children = undefined;
            // node = { ...node, children: undefined };
        }
    });

    return source;
};

const toggleChildren = (id: string, state: boolean, listNodes: Record<string, NodeProps>) => {
    const node = listNodes[id];
    node.checked = state;

    if (!isEmpty(node._children)) {
        node._children.forEach((id: string) => toggleChildren(id, state, listNodes));
    }
};

const unCheckParents = (node: NodeProps, listNodes: Record<string, NodeProps>) => {
    let parent = node.parent;
    while (parent) {
        const next = listNodes[parent];
        next.checked = false;
        parent = next.parent;
    }
};

const collapseChildren = (node: NodeProps, listNodes: Record<string, NodeProps>) => {
    node.expanded = false;
    if (!isEmpty(node._children)) {
        node._children.forEach((c: string | number) => collapseChildren(listNodes[c], listNodes));
    }
};

export const toggleNodeExpandState = (id: string, listNodes: Record<string, NodeProps>) => {
    const node = listNodes[id];
    node.expanded = !node.expanded;
    if (!node.expanded) collapseChildren(node, listNodes);
    return listNodes;
};

export const setNodeCheckedState = (id: string, checked: boolean, listNodes: Record<string, NodeProps>) => {
    const node = listNodes[id];
    node.checked = checked;
    toggleChildren(id, checked, listNodes);
    if (checked && node.parent) {
        const parent = listNodes[node.parent];
        if (!isEmpty(parent._children)) {
            let checkStateParent = true;
            parent._children.forEach((c: string | number) => {
                if (!listNodes[c].checked || listNodes[c].checked === false) checkStateParent = false;
            });
            parent.checked = checkStateParent;
        }
    }
    if (!checked) {
        unCheckParents(node, listNodes);
    }
    return listNodes;
};

export const addParentsToTree = (
    parentId: string,
    listNodes: Record<string, NodeProps>,
    searchList: Array<NodeProps>
    // nodesSearch: Record<string, NodeProps>
) => {
    if (parentId !== undefined) {
        const nodeParent = listNodes[parentId];
        // nodesSearch[id] = { ...node };
        searchList.push(nodeParent);
        if (nodeParent.parent) {
            addParentsToTree(nodeParent.parent, listNodes, searchList);
        }
    }
};
