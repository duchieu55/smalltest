import Axios from 'axios';
import React from 'react';
import { INDENT_WIDTH, SUCCESS_STATUS } from '../../../../constants/contants';
import { isEmpty } from '../../../../utils';
import Spinner from '../../../Spinner';
import { isLeaf } from '../../utils';
import NodeLabel from './components/NodeLabel';
import Toggle from './components/Toggle';

export interface NodeProps {
    id: string;
    // key: string;
    depth?: number;
    children?: NodeProps[];
    _children: string[];
    childrenIndex?: number;
    parent?: string;
    title?: string;
    label: string;
    value: string;
    checked: boolean;
    expanded?: boolean;
    disabled?: boolean;
    display?: boolean;
    clientId?: string;
    onNodeToggle: (id: string) => void;
    onAction: void;
    onLoadMore: (id: string) => void;
    onCheckboxChange: (id: string, checked: boolean) => void;
    loading: boolean;
    fetching: boolean;
    isLastChildren: boolean;
}

interface Props {
    node: NodeProps;
    searchModeOn: boolean;
}

const TreeNode: React.FC<Props> = ({ node, searchModeOn }) => {
    const {
        id,
        depth,
        children,
        title,
        label,
        value,
        parent,
        checked,
        expanded,
        clientId,
        loading,
        isLastChildren,
        onLoadMore,
        onNodeToggle,
        onCheckboxChange,
    } = node;
    const style = { paddingLeft: `${(depth || 0) * INDENT_WIDTH}px` };
    const liId = `${id}_li`;
    const isLeafNode = isLeaf(children || []);

    const getMoreData = async (): Promise<void> => {
        onLoadMore(id);
    };

    return (
        <li key={id} className="node tree" style={style} id={liId}>
            {!searchModeOn && <Toggle isLeaf={isLeafNode} expanded={expanded} id={id} onNodeToggle={onNodeToggle} />}
            <NodeLabel
                title={title}
                label={label}
                id={id}
                checked={checked}
                value={value}
                onCheckboxChange={onCheckboxChange}
                clientId={clientId}
            />
            {loading && <Spinner />}
            {isLastChildren && !loading && !searchModeOn && (
                <div
                    className="loadmore"
                    onClick={(event: React.MouseEvent<HTMLElement>) => {
                        getMoreData();
                    }}
                >
                    Load more
                </div>
            )}

            {/* <Actions actions={actions} onAction={onAction} id={_id} readOnly={readOnly} /> */}
        </li>
    );
};

export default TreeNode;
