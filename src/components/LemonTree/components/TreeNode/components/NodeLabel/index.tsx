import React from 'react';

interface Props {
    id: string;
    title?: string;
    label: string;
    value: string;
    checked: boolean;
    onCheckboxChange: (id: string, checked: boolean) => void;
    clientId?: string;
}

const NodeLabel: React.FC<Props> = ({ id, title, label, value, checked, clientId, onCheckboxChange }) => {
    const onChange = (event: React.ChangeEvent<HTMLInputElement>) => {
        onCheckboxChange(id, event.target.checked);
    };
    return (
        <label title={title || label} htmlFor={id}>
            <input type="checkbox" onChange={onChange} checked={checked} value="" />
            <span className="node-label">{label}</span>
        </label>
    );
};

export default NodeLabel;
