import React from 'react';

interface Props {
    id: string;
    expanded?: boolean;
    isLeaf?: boolean;
    onNodeToggle: (id: string) => void;
}

const Toggle: React.FC<Props> = ({ id, expanded, isLeaf, onNodeToggle }) => {
    const toggleCx = expanded ? 'fa-minus' : 'fa-plus';

    if (isLeaf) {
        return <i className="toggle fa fa-plus" style={{ visibility: 'hidden' }} aria-hidden />;
    }

    const onToggle = (event: {
        stopPropagation: () => void;
        nativeEvent: { stopImmediatePropagation: () => void };
    }) => {
        event.stopPropagation();
        event.nativeEvent.stopImmediatePropagation();
        onNodeToggle(id);
    };

    return <i role="button" tabIndex={-1} className={`toggle fa ${toggleCx}`} aria-hidden="true" onClick={onToggle} />;
};

export default Toggle;
