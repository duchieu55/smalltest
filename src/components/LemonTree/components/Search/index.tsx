import React, { useEffect, useRef, useState } from 'react';
import useDebounce from '../../../../hook/useDebounce';
import { isEmpty } from '../../../../utils';
import './style.scss';

interface Props {
    onSearch: (searchTerm: string) => void;
}

const Search: React.FC<Props> = ({ onSearch }) => {
    const [searchTerm, setSearchTerm] = useState<string>('');
    const prevSearchTermRef = useRef();
    const debouncedSearchTerm: string = useDebounce<string>(searchTerm, 500);

    const handleInputChange = (event: React.ChangeEvent<HTMLInputElement>) => {
        setSearchTerm(event.target.value);
    };

    const onKeyDown = (e: React.KeyboardEvent<HTMLInputElement>) => {
        if (e.key == 'Enter') {
            debugger;
        }
    };

    useEffect(() => {
        if (debouncedSearchTerm || (isEmpty(debouncedSearchTerm) && prevSearchTermRef.current)) {
            console.log('start search !');
            onSearch(searchTerm);
        } else {
            console.log('set result !');
        }
        prevSearchTermRef.current = debouncedSearchTerm;
    }, [debouncedSearchTerm]);

    return (
        <input
            type="text"
            className="search"
            placeholder="Search..."
            onKeyDown={onKeyDown}
            onChange={handleInputChange}
        />
    );
};

export default Search;
