import React, { useCallback, useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { NODE_PREFIX_ID, SUCCESS_STATUS } from '../../constants/contants';
import { RootState } from '../../redux/store';
import { fetchTreeData } from '../../redux/tree/actions';
import { isEmpty, isEmptyObject } from '../../utils';
import Search from './components/Search';
import TreeNode, { NodeProps } from './components/TreeNode';
import clonedeep from 'lodash/clonedeep';
import './style.scss';

import {
    addParentsToTree,
    flattenTree,
    isLastChildren,
    setNodeCheckedState,
    shouldRenderNode,
    toggleNodeExpandState,
} from './utils';
import Axios from 'axios';
import cloneDeep from 'lodash/clonedeep';

const Tree: React.FC = () => {
    const dispatch = useDispatch();
    const { treeInfo } = useSelector((state: RootState) => state.tree);
    const [listNodesTree, setListNodesTree] = useState<Record<string, NodeProps>>({});
    const [listNodesSearch, setListNodesSearch] = useState<Record<string, NodeProps>>({});
    const [searchModeOn, setSearchModeOn] = useState<boolean>(false);

    useEffect(() => {
        dispatch(fetchTreeData());
        const listNode = flattenTree(treeInfo, NODE_PREFIX_ID);
        setListNodesTree({ ...listNode });
    }, []);

    const onNodeToggle = useCallback(
        (id: string) => {
            const newTreeNodes = clonedeep(listNodesTree) as Record<string, NodeProps>;
            const node = newTreeNodes[id];
            if (!node.expanded) {
                node.loading = true;
                setListNodesTree({ ...newTreeNodes });
                setTimeout(() => {
                    toggleNodeExpandState(id, newTreeNodes)[id].loading = false;
                    setListNodesTree(newTreeNodes);
                }, 300);
            } else {
                setListNodesTree(toggleNodeExpandState(id, newTreeNodes));
            }
        },
        [listNodesTree]
    );

    const generateNodesUI = () => {
        if (isEmptyObject(searchModeOn ? listNodesSearch : listNodesTree)) {
            return <p>Data not found</p>;
        }
        return Object.values(searchModeOn ? listNodesSearch : listNodesTree).map((node) => {
            const isDisplay = shouldRenderNode(node, listNodesTree, searchModeOn);
            if (isDisplay) {
                node.isLastChildren = isLastChildren(node, listNodesTree);
                const treeNode = { ...node, onNodeToggle, onCheckboxChange, onLoadMore };
                return (
                    <>
                        <TreeNode node={treeNode} searchModeOn={searchModeOn} />
                    </>
                );
            }
        });
    };

    const onCheckboxChange = useCallback(
        (id: string, checked: boolean) => {
            setListNodesTree({ ...setNodeCheckedState(id, checked, listNodesTree) });
        },
        [listNodesTree]
    );

    const onSearch = useCallback(
        (searchTerm: string) => {
            if (!isEmpty(searchTerm)) {
                setSearchModeOn(true);
            } else {
                setSearchModeOn(false);
            }
            const searchRs = Object.values(listNodesTree).filter((obj) => obj.value.includes(searchTerm));
            let nodesSearch: Record<string, NodeProps> = {};
            let searchList: Array<NodeProps> = [];
            searchRs.forEach((node, i) => {
                searchList.sort((a, b) => {
                    if (typeof a.id === 'undefined' || typeof b.id === 'undefined') {
                        return 0;
                    }
                    return a.id > b.id ? 1 : -1;
                });
                searchList.push(node);
                if (node.parent) addParentsToTree(node.parent, listNodesTree, searchList);
            });
            searchList.forEach((node) => {
                nodesSearch[node.id] = { ...node };
            });
            setListNodesSearch({ ...nodesSearch });
        },
        [listNodesTree]
    );

    const onLoadMore = async (id: string) => {
        try {
            const response = await Axios.get('http://localhost:3030/api/loadmore');
            if (response.data.result === SUCCESS_STATUS) {
                const newTreeNodes = cloneDeep(listNodesTree);
                const lastNode = newTreeNodes[id];
                lastNode.loading = true;
                setListNodesTree({ ...newTreeNodes });
                const parentNode = newTreeNodes[lastNode.parent || ''];
                const arrNodes = Object.values(newTreeNodes);
                const newNodes = response.data.reply.map((_node: any, i: number) => {
                    _node.id = `${parentNode.id}-${(lastNode.childrenIndex || 0) + (i + 1)}`;
                    _node.parent = parentNode.id;
                    _node.depth = lastNode.depth;
                    _node.childrenIndex = (lastNode.childrenIndex || 0) + (i + 1);
                    parentNode._children.push(_node.id);
                    return _node;
                });
                arrNodes.splice(arrNodes.findIndex((x) => x.id === lastNode.id) + 1, 0, ...newNodes);
                let source: Record<string, NodeProps> = {};
                arrNodes.forEach((node, i) => {
                    source[node.id] = { ...node };
                });
                setTimeout(() => {
                    source[id].loading = false;
                    setListNodesTree(source);
                }, 300);
            }
        } catch (err) {
            // handle error
        }
    };

    return (
        <>
            <Search onSearch={onSearch} />
            <br />
            <ul className="checktree">{generateNodesUI()}</ul>
        </>
    );
};

export default Tree;
