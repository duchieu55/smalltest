const isEmpty = (o: string | any[]) => !o || (Array.isArray(o) && !o.length);

export default isEmpty;

const isEmptyObject = (obj: {}) => {
    return Object.keys(obj).length === 0;
};
