import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import { PersistGate } from 'redux-persist/integration/react';
import { store, persist } from './redux/store';
import { App } from './App';

ReactDOM.render(
    <Provider store={store}>
        <PersistGate loading={null} persistor={persist}>
            <App />
        </PersistGate>
    </Provider>,
    document.getElementById('root')
);
