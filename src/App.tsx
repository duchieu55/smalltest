import './App.css';
import Tree from './components/LemonTree';

export const App = () => {
    return (
        <div className="wrapper">
            <div className="tree-container">
                <h2>AI Active Small Test</h2>
                <Tree />
            </div>
        </div>
    );
};
